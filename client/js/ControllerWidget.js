
function inheritPrototype(childObject, parentObject) {
	//http://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/
    var copyOfParent = Object.create(parentObject.prototype);
    copyOfParent.constructor = childObject;
   	childObject.prototype = copyOfParent;
}

function ControllerWidget(name,layer) {
	this.widgetName = name;
	this.layer = layer;
	this.touches = {};
	this.tweens = {};
}
ControllerWidget.prototype.up = function(touch) {};
ControllerWidget.prototype.down = function(touch) {};
ControllerWidget.prototype.enter = function(touch) {};
ControllerWidget.prototype.exit = function(touch) {};
ControllerWidget.prototype.move = function(touch) {};

function Controller() {
	this.controllerState = {};
	this.touchPool = {};
	this.network = null;
}

/**
 * @param {Object} config
 * @param {String} [config.name] name of the widget
 * @param {String} [config.layer] the layer it
 * @param {String} [config.controller] the controller to attach to
 */
function CWidget(config) {
	this.widgetName = config.name;
	this.layer = config.layer;
	this.controller = config.controller;
	this.touches = {};
	this.tweens = {};
}

function CButton(config) {
	CWidget.call(this, config);
	this.shape = config.shape;
	this.shape.setName(this.name);
}
inheritPrototype(CButton, CWidget);

function CTouch(touchEvt) {
	this.evt = touchEvt;
	this.widget = null;
}
RCTouch.prototype.getPageX = function() {
	return this.evt.pageX;
};
RCTouch.prototype.getPageY = function() {
	return this.evt.pageY;
};

function PathSurface(config) {
	CWidget.call(this, config);
	this.shapes = config.shapes;

}