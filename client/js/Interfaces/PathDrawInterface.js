var PathDrawInterface = function() {

	//name
	this.nameId = "PathDraw";

	this.layermanager = null;
	this.touchdispatcher = null;

	this.init = function(layermanager, touchdispatcher) {
		this.layermanager = layermanager;
		this.touchdispatcher = touchdispatcher;
		this.initInterface();
	}

	//background image

	//put on stage
	////put with name specified above
	this.addToStage = function() {
		var stage = this.layermanager.stage;
		stage.add(this.hitLayer);
		stage.add(this.pathLayer);
		this.touchdispatcher.addAsListener(this.hitLayer);
		this.hitLayer.draw();
		this.pathLayer.draw();
	};

	//remove from stage
	////removes by name specified above
	this.removeFromStage = function() {
		var stage = this.layermanager.stage;
		this.touchdispatcher.removeAsListener(this.hitLayer);
		var layer = stage.find('#PathDrawPath');
		if(layer.length == 1) {
			layer[0].remove();
		}
		layer = stage.find('#PathDrawHitLayer');
		if(layer.length == 1) {
			layer[0].remove();
		}
	};


	//define layers and GUI
	this.initInterface = function() {
		var layermanager = this.layermanager;
		var sizeInfo = this.layermanager.autostage.sizeInfo();

		this.pathLayer = new Kinetic.Layer({id: "PathDrawPath", listening:false});
		this.hitLayer = stampit.mixIn(new Kinetic.Layer({id: "PathDrawHitLayer"}), LayerTouchHandlerDefault);

	    var prevPoint = {x: -100, y:-100};
	    var pathLayer = this.pathLayer;

	    var pathObject = {
	      isDrawing: false,
	      isComplete: false,
	      pathLayer: this.pathLayer,
	      clearPoints: function() { 
	        var counter = 0; 
	        while(this.getPoints().length) { 
	          counter++; 
	          this.getPoints().pop(); 
	        } 
	      },
	      refreshPath: function() { this.fire('pointsChange'); }
	    };
	    var pathLineConfig = {
	      stroke: '#EAD061',//,'#A7C4AD'
	      strokeWidth: 3,
	      points: [],
	      tension: 0.5
	    };
	    var pathLine = stampit.mixIn(new Kinetic.Line(pathLineConfig), pathObject);  
	    // setInterval(function() { 
	    //   pathLine.tension(0.5 + ((Math.random() * 2) - 1) * 0.05);
	    //   pathLine.fire('pointsChange');
	    //   pathLayer.batchDraw();
	    // }, 100);  
	    var endZoneConfig = {
	      id: "endZone",
	      x: sizeInfo.stageWidth - 40,
	      y:0,
	      width: 40,
	      height: sizeInfo.stageHeight,
	      fill: '#657665',
	      fillAlpha: 0.3
	    };
	    var endZoneTouchHandler = {
	      enter: function(touch, isNotification) {
	        if(pathLine.isDrawing) {
	          pathLine.isComplete = true;
	          pathLine.isDrawing = false;
	          var touchPos = { 
	            x: touch.stageX,
	            y: touch.stageY
	          };
	          var points = pathLine.getPoints();
	          points.push(touchPos.x);
	          points.push(touchPos.y);
	          pathLine.refreshPath();
	          pathLayer.draw();
	        }
	      },
	      up: function() {},
	      down: function() {},
	      exit: function() {}
	    };
	    var endZone = stampit.mixIn(new Kinetic.Rect(endZoneConfig), endZoneTouchHandler);  
	    var startZoneConfig = {
	      id: "startZone",
	      radius: 5,
	      fill: '#FF5C31', //red',
	    };
	    var startZoneTouchHandler = {
	      down: function(touch, isNotification) {
	        pathLine.clearPoints();
	        var pathPoints = pathLine.getPoints();
	        pathPoints.push(this.startingPoint.x); //initial X/Y
	        pathPoints.push(this.startingPoint.y);
	        pathLine.refreshPath();
	        pathLine.isComplete = false;
	        pathLine.isDrawing = true;
	        pathLayer.draw();
	      },
	      up: function() {},
	      exit: function() {},
	      enter: function() {},
	      move: function() {}
	    };
	    var startZoneHitCircleConfig = {
	      id: "startZoneHit",
	      radius: 25,
	      fillRed: 255,
	      fillGreen: 0,
	      fillBlue: 0,
	      fillAlpha: 0.3
	    };
	    var startZone = stampit.mixIn(new Kinetic.Circle(startZoneConfig));
	    var startZoneHit = stampit.mixIn(new Kinetic.Circle(startZoneHitCircleConfig), startZoneTouchHandler);  
	    var pathCanvasConfig = {
	      x: 0, y: 0,
	      width: sizeInfo.stageWidth,
	      height: sizeInfo.stageHeight,
	      fill: '#2C2625'//'#262626'
	    };
	    var pathCanvasState = {
	      prevX: -1,
	      prevY: -1
	    };
	    var pathCanvasTouchHandler = {
	      move: function(touch, isNotification) {
	        if(this.prevX < 0) { //initial move
	          var points = pathLine.getPoints();
	          points.push(touch.stageX);
	          points.push(touch.stageY);
	          this.prevX = touch.stageX;
	          this.prevY = touch.stageY;
	          pathLine.refreshPath();
	          pathLayer.draw();
	          return;
	        }
	        if(pathLine.isDrawing
	          && (Math.pow(touch.stageX - this.prevX,2) + Math.pow(touch.stageY - this.prevY, 2) > Math.pow(30,2))
	          ) {
	          var points = pathLine.getPoints();
	          points.push(touch.stageX);
	          points.push(touch.stageY);
	          this.prevX = touch.stageX;
	          this.prevY = touch.stageY;
	          pathLine.refreshPath();
	          pathLayer.draw();  
	          //log("PLACING POINTS");
	        }
	        //log("moving");
	      },
	      up: function(touch, isNotification) {
	        pathLine.isDrawing = false;
	        pathLine.isComplete = false;
	      },
	      down: function() {
	      },
	      exit: function() {},
	      enter: function() {}
	    };
	    var pathCanvas = stampit.mixIn(new Kinetic.Rect(pathCanvasConfig), pathCanvasState, pathCanvasTouchHandler);  
	    var clearButtonConfig = { };
	    var sendButtonState = {
	      xPosition: 50,
	      yPosition: 300,
	      isArmed: false
	    };
	    var sendButtonConfig = { //Rect
	      width: 50, height: 50,
	      fill: 'yellow'
	     };
	    var sendButtonTouchHandler = {
	      up: function(touch, isNotification) {
	        var i, points, normalizedPoints, stageInfo;
	        if(this.isArmed) {
	          this.isArmed = false;
	          points = pathLine.getPoints();
	          normalizedPoints = [];
	          stageInfo = sizeInfo;
	          for(i = 0; i < points.length - 1; i+=2) {
	            normalizedPoints.push(points[i] / stageInfo.stageWidth);
	            normalizedPoints.push(points[i+1] / stageInfo.stageHeight);
	          }
	          console.log(window.aipnetwork);
	          console.log(JSON.stringify(normalizedPoints));
	          CommunicationModule.sendPath(JSON.stringify(normalizedPoints));
	        }
	      },
	      down: function(touch, isNotification) {
	        this.isArmed = true;
	      },
	      enter: function(touch, isNotification) {
	      },
	      exit: function(touch, isNotification) { 
	      }
	    };
	    var sendButtonMethods = { 
	      hideButton: function() {
	        this.x(-500);
	        this.y(-500);
	      },
	      showButton: function(x, y) {
	        this.x(x);
	        this.y(y);
	      }
	    };
	    var sendButton = stampit.mixIn(new Kinetic.Rect(sendButtonConfig), sendButtonState, sendButtonMethods, sendButtonTouchHandler);
	    sendButton.showButton(20,300);  
	    var ElementTouchHandler = {
	      up: function(touch, isNotification) {
	      },
	      down: function(touch, isNotification) {
	      },
	      enter: function(touch, isNotification) {
	      },
	      exit: function(touch, isNotification) { 
	      }
	    };  
	    var ControllerElement = stampit().state({ 
	        //state
	        shape: null,
	        controllerLayer: null, //parent
	        touches: [],
	        numTouches: 0
	    });  
	    var textpath = new Kinetic.TextPath({
		    x: 100,
		    y: 50,
		    fill: '#657665',
		    fontSize: '24',
		    fontFamily: 'Arial',
		    text: 'All the world\'s a stage, and all the men and women merely players.',
		    data: 'M10,10 C0,0 10,150 100,100 S300,150 400,50',
		    listening: false
		});

	  this.hitLayer.add(pathCanvas);
	  this.hitLayer.add(startZone);
	  this.hitLayer.add(startZoneHit);
	  this.hitLayer.add(endZone);
	  this.hitLayer.add(textpath);
	  this.hitLayer.add(sendButton);
	  this.pathLayer.add(pathLine);

		var button_continue_config = {
			x: sizeInfo.stageWidth-150, y: sizeInfo.stageHeight-50,
			width: 100, height: 50,
			fill: 'yellow'
		};
		var button_continue_touch = {
		    up: function(touch, isNotification) {
		    	console.log("button");
		    	layermanager.gotoInterface("SitBackAndRelaxScreen")
		    },
		    down: function(touch, isNotification) {
		    },
		    enter: function(touch, isNotification) {
		    },
		    exit: function(touch, isNotification) { 
		    },
		    move: function(touch, isNotification) { 
		    }
		}
		var button_continue = new Kinetic.Rect(button_continue_config);
		stampit.mixIn(button_continue, button_continue_touch);

		this.hitLayer.add(button_continue);


	};

	this.set = function(values) {
		var sizeInfo = this.layermanager.autostage.sizeInfo();
		var x = values.x * sizeInfo.stageWidth;
		var y = values.y * sizeInfo.stageHeight;
		var startZone = this.hitLayer.find("#startZone")[0];
		var startZoneHit = this.hitLayer.find("#startZoneHit")[0];
		startZone.x(x);
		startZone.y(y);
		startZoneHit.x(x);
		startZoneHit.y(y);
		startZoneHit.startingPoint = {x: x, y: y};
		var endZone = this.hitLayer.find("#endZone")[0];
		//endZone.x(0);
	}
};