var SitBackAndRelaxInterface = function() {

	//name
	this.nameId = "SitBackAndRelaxScreen";

	this.layermanager = null;
	this.touchdispatcher = null;

	this.init = function(layermanager, touchdispatcher) {
		this.layermanager = layermanager;
		this.touchdispatcher = touchdispatcher;
		this.initInterface();
	}

	//background image

	//put on stage
	////put with name specified above
	this.addToStage = function() {
		var stage = this.layermanager.stage;
		this.touchdispatcher.addAsListener(this.layer);
		stage.add(this.layer);
	};

	//remove from stage
	////removes by name specified above
	this.removeFromStage = function() {
		var stage = this.layermanager.stage;
		this.touchdispatcher.removeAsListener(this.layer);
		var layer = stage.find('#' + this.nameId);
		if(layer.length == 1) {
			layer[0].remove();
		}
	};


	//define layers and GUI
	this.initInterface = function() {
		var sizeInfo = this.layermanager.autostage.sizeInfo();
		this.layer = new Kinetic.Layer({id: this.nameId});
		stampit.mixIn(this.layer, LayerTouchHandlerDefault);

		var layermanager = this.layermanager;

		var button_continue_config = {
			x: sizeInfo.stageWidth-150, y: sizeInfo.stageHeight-50,
			width: 100, height: 50,
			fill: 'pink'
		};
		var button_continue_touch = {
		    up: function(touch, isNotification) {
		    	console.log("button");
		    	layermanager.gotoInterface("RoundStartScreen")
		    },
		    down: function(touch, isNotification) {
		    },
		    enter: function(touch, isNotification) {
		    },
		    exit: function(touch, isNotification) { 
		    },
		    move: function(touch, isNotification) { 
		    }
		}
		var button_continue = new Kinetic.Rect(button_continue_config);
		stampit.mixIn(button_continue, button_continue_touch);

		this.layer.add(button_continue);

		var simpleText = new Kinetic.Text({
	        x: sizeInfo.stageWidth / 3,
	        y: sizeInfo.yToCenter,
	        text: "Sit Back and Relax",
	        fontSize: 50,
	        fontFamily: 'Calibri',
	        fill: 'white'
		});

		this.layer.add(simpleText);
	}

};