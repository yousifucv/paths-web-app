var LayerSequenceModule = (function() {

	this.currentState = null;

	this.states = ["TitleScreen",
					"Mobicode",
					"Instructions",
					"RoundLoading",
					"RoundDraw",
					"RoundDrawEnd",
					"RoundPlay",
					"RoundEnd",
					"GameEnd",
					"Credits"];

	var init = function(aipnetwork) {

	};
					

	var publicAPI = {
		init: init
	};

	return publicAPI;

})();