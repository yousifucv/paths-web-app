var NetworkEvents = (function(networkEvents){

	networkEvents.JoinGame = {
		name: "JoinGame"
	};
	networkEvents.GameInstructions = {
		name: "GameInstructions"
	};
	networkEvents.RoundLoading = {
		name: "RoundLoading",
		number: null
	};
	networkEvents.RoundDraw = {
		name: "RoundDraw"
	};
	networkEvents.RoundDrawEnd = {
		name: "RoundDrawEnd"
	};
	networkEvents.RoundPlay = {
		name: "RoundPlay"
	};
	networkEvents.RoundEnd = {
		name: "RoundEnd"
	};
	networkEvents.GameEnd = {
		name: "GameEnd"
	};
	networkEvents.GameCredits = {
		name: "GameCredits"
	};

	networkEvents.Discovery = {
		name: "Discovery"
	};
	networkEvents.Redisocvery = {
		name: "Rediscovery"
	};
	networkEvents.Ping = {
		name: "Ping",
		clientTime: 1
	};
	networkEvents.PingBack = {
		name: "PingBack",
		clientTime: 1,
		serverTime: 1
	};

	return networkEvents;

})(NetworkEvents || {});