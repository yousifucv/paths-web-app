//Singleton
var NetworkEventHandler = function(networkEvents) {

	this.networkEvents = networkEvents || {};
	this.handlers = {};

	this.register = function(aipnetwork) {
		for(ver key in this.networkEvents) {
			aipnetwork.on(key, this.handlers[key + "Handler"]);
		}
	};

};