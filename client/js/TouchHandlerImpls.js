var LayerTouchHandlerDefault = {
	touchstart: function(evt, touch) {
	  var touchPos = {
	    x: touch.hitX,
	    y: touch.hitY
	  };
	  var shape = this.getIntersection(touchPos);
	  if(shape) {
	    touch.shape = shape;
	    shape.down(touch);
	  }
	  touch.notifyDown();
	},
	touchend: function(evt, touch) {
	  var touchPos = {
	    x: touch.hitX,
	    y: touch.hitY
	  };
	  //log("hit: " + touch.hitX + " " + touch.hitY + " stage: " + touch.stageX + " " + touch.stageY);
	  var shape = this.getIntersection(touchPos);

	  if(shape && shape == touch.shape) {
	    touch.shape.up(touch);
	    touch.shape = null;
	  }
	  //log("dispatching notify up");
	  touch.notifyUp();
	},
	touchmove: function(evt, touch) {
	    var touchPos = {
	      x: touch.hitX,
	      y: touch.hitY
	    };
	    var shape = this.getIntersection(touchPos);
	    if(shape) {
	      if(touch.shape == null) {
	        //log("entered a shape");
	        touch.shape = shape;
	        shape.enter(touch);
	        shape.move(touch);
	        //log("handled enter");
	      } else if(touch.shape != shape) {
	        touch.shape.exit(touch);
	        touch.shape = shape;
	        shape.enter(touch);
	        shape.move(touch);
	        //log("handled shape swap");
	      } else {
	        shape.move(touch);
	      }
	    } else { //no shape intersection
	      if(touch.shape !== null) {
	        touch.shape.exit(touch);
	        touch.shape = null;
	        //log("handled exit");
	      }
	    }
	    touch.notifyMove();
	},
	touchcancel: function(evt, touch) {
	    var touchPos = {
	      x: touch.hitX,
	      y: touch.hitY
	    };
	    var shape = this.getIntersection(touchPos);
	    if(shape) {
	      touch.shape.up(touch);
	      touch.shape = undefined;
	    }
	    touch.notifyUp();
	} 
};

var ElementTouchHandler = {
    up: function(touch, isNotification) {
    },
    down: function(touch, isNotification) {
    },
    enter: function(touch, isNotification) {
    },
    exit: function(touch, isNotification) { 
    },
    move: function(touch, isNotification) { 
    }
 };

 var LayerTouchHandler = {
	touchstart: function(evt, touch) {
	},
	touchend: function(evt, touch) {
	},
	touchmove: function(evt, touch) {
	},
	touchcancel: function(evt, touch) {
	} 
};