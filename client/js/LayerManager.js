//Singleton
var LayerManager = function(autostage, touchdispatcher) {

	this.autostage = autostage;
	this.stage = null; //assigned below
	this.touchdispatcher = touchdispatcher;

	this.interfaces = {};
	this.currentInterface = null;

	this.init = function() {
		//needs to happen in init() because it is not define 
		//when it is first run during the initial app loading
		//because the autostage has not done its initialization
		this.stage = this.autostage.stage(); 
	};

	//Internal
	//gets the hit layer of the stage (top layer, named "HitLayer")
	var _getHitLayer = function(self) {
		var hitLayer = self.stage.find("#HitLayer");
		if(hitLayer.length == 1) {
			return hitLayer[0];
		} else {
			return null;
		}
	};

	//each interface should call this when it defines itself
	this.importInterface = function(interfaceObj) {
		interfaceObj.init(this, touchdispatcher);
		this.interfaces[interfaceObj.nameId] = interfaceObj;
	}

	//Add a layer to the stage.
	this.addInterfaceToStage = function(name) {
		var interfaceObj = this.interfaces[name];
		interfaceObj.addToStage(this, this.touchdispatcher);
		this.currentInterface = interfaceObj;
		//now we make sure to move the hit layer to the top
		//so it can recieve touch events
		var hitLayer = _getHitLayer(this);
		if(hitLayer) {
			hitLayer.moveToTop();
		}
	};

	//called by the LayerSequenceModule
	this.removeInterfaceFromStage = function(name) {
		var interfaceObj = this.interfaces[name];
		interfaceObj.removeFromStage();
		this.currentInterface = null;

	};

	//goto the specified interface and remove anything that's there
	this.gotoInterface = function(name) {
		if(this.currentInterface != null) {
			this.removeInterfaceFromStage(this.currentInterface.nameId);
		}
		this.addInterfaceToStage(name);
	};

	this.set = function(name, values) {
		var interfaceObj = this.interfaces[name];
		if(interfaceObj && interfaceObj.set) {
			interfaceObj.set(values);	
		} else {
			console.log("Bad interface name!");
		}
	};

}