//Singleton
var TouchDispatcher = function(touchManager) {

	this.listeners = [];

	this.handleStart = function(evt) {
		var i, j;
		for (i = 0; i < evt.changedTouches.length; i++) {
	        var touch = touchManager.put(evt.changedTouches[i]);
			for(j = 0; j < this.listeners.length; j++) {
				this.listeners[j].touchstart(evt, touch);
			}
	    }
	}
	
	this.handleMove = function(evt) {
		var i, j;
	    for (i = 0; i < evt.changedTouches.length; i++) {
	        var id = evt.changedTouches[i].identifier;
	        var touch = touchManager.updateAtNativeId(evt.changedTouches[i], id);
			for(j = 0; j < this.listeners.length; j++) {
				this.listeners[j].touchmove(evt, touch);
			}
		}
	}

	this.handleEnd = function(evt) {
		var i, j;
		for (i = 0; i < evt.changedTouches.length; i++) {
	        var id = evt.changedTouches[i].identifier;
	        var touch = touchManager.removeByNativeId(id);
			for(j = 0; j < this.listeners.length; j++) {
				this.listeners[j].touchend(evt, touch);
			}
		}
	}

	this.handleCancel = function(evt) {
		var i, j;
		for (i = 0; i < evt.changedTouches.length; i++) {
	        var id = evt.changedTouches[i].identifier;
	        var touch = touchManager.removeByNativeId(id);
			for(j = 0; j < this.listeners.length; j++) {
				this.listeners[j].touchend(evt, touch);
			}
		}
	}

	this.addAsListener = function(layer) {
		this.listeners.push(layer);
	}

	this.removeAsListener = function(layer) {
		var i;
		for(i = 0; i < this.listeners.length; i++) {
			if(this.listeners[i] == layer) {
				this.listeners.splice(i,1); //remove the listener
			}
		}
	}


}