var ConnectionManager = function(aipnetwork) {

	this.aipnetwork = aipnetwork;
	this.service = null; //timeout handle to be set below

	this.connect = function(mobicode) {
		this.aipnetwork.connect(mobicode);
		this.startDiscovery();
	}

	this.startDiscovery = function() {
		this.service = window.setTimeout(function() {
			this.aipnetwork.send(NetworkEvents.Discovery, "");
		}, 1000);
	}

	this.stopDiscovery = function() {
		window.cancelTimeout(this.service);
	}

}