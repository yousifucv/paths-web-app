//http://www.wildbunny.co.uk/blog/2012/11/20/how-to-make-a-multi-player-game-part-2/
var PingModule = (function(window) {

	var _network = null;
	var _timeout = null;
	var _msgNames = null;
	var _avgPing = 0;
	var _timeCompensation = 100000000;

	var init = function(network, msgNames) {
		_network = network;
		_msgNames = msgNames;
		_network.on( _msgNames.PingBack.name, pingBackHandler);

		_timeout = window.setInterval( ping, 5000 );
		console.log("PingModule init complete");
	}

	var pingBackHandler = function(packet) {
		var initialTime = packet.clientTime;
		var serverTime = packet.serverTime;
		var currentTime = new Date().getTime();

		var roundTripTime = currentTime - initialTime;
		var latency = 0.5 * roundTripTime;

		//difference between our current time and the servers time (without compensating for latency)
		var serverDeltaTime = serverTime - currentTime;

		_avgPing = 0.5 * ( _avgPing + latency );

		var timeCompensation = serverDeltaTime + _avgPing;
		if (timeCompensation * timeCompensation < _timeCompensation * _timeCompensation) {
			_timeCompensation = timeCompensation;
		}
		// console.log("Latency: " + latency);
		// console.log("initialTime: " + initialTime);
		// console.log("currentTime: " + currentTime);
		// console.log("serverTime: " + serverTime);
		console.log("timeCompensation: " + _timeCompensation);
		console.log("ping: " + _avgPing);

		console.log("Processing time: " + (new Date().getTime() - currentTime));

	}

	var ping = function() {
		var pingObj = {
			clientTime: new Date().getTime()
		}
		_network.send( _msgNames.Ping.name, JSON.stringify( pingObj ) );
	}

	var serverTime = function() {
		return (new Date().getTime()) + _timeCompensation;
	}

	var avgPing = function() {
		return _avgPing;
	}


	var publicAPI = {
		init: init,
		getServerTime: serverTime,
		avgPing: avgPing
	}

	return publicAPI;

})(window);