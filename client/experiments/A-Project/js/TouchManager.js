//Singleton
var TouchManager = function(stage) {
	var i;
	var touchArraySize = 10;

	this.size = 0;
	this.ongoingTouches = [];
	this.itIndex = 0; //iterator state position

	this.mixInInstanceMethods = {
		notifyUp: function() {
			var i, len = this.observers.length;
			for(i = 0; i < len; i++) {
				this.observers[i].up(this, true);
			}
		},
		notifyDown: function() {
			var i, len = this.observers.length;
			for(i = 0; i < len; i++) {
				this.observers[i].down(this, true);
			}
		},
		notifyExit: function() {
			var i, len = this.observers.length;
			for(i = 0; i < len; i++) {
				this.observers[i].exit(this, true);
			}
		},
		notifyEnter: function() {
			var i, len = this.observers.length;
			for(i = 0; i < len; i++) {
				this.observers[i].enter(this, true);
			}
		},
		notifyMove: function() {
			var i, len = this.observers.length;
			for(i = 0; i < len; i++) {
				this.observers[i].move(this, true);
			}
		},
		notifyCancel: function() {
			var i, len = this.observers.length;
			for(i = 0; i < len; i++) {
				this.observers[i].cancel(this, true);
			}
		},
		addObserver: function(shape) {
			this.observers.push(shape);
		},
		removeObserver: function(shape) {
			var i;
			for(i = 0; i < this.observers.length; i++) {
				if(this.observers[i] == shape) {
					//window.logg("Spliced");
					return this.observers.splice(i, 1)[0];
				}
			}
		}
	};

	this.mixInState = {
		shape: null,
		observers: [],
		nativeId: null,
		pageX: 0,
		pageY: 0,
		stageX: 0,
		stageY: 0,
		hitX: 0,
		hitY: 0
	};

	for(i = 0; i < touchArraySize; i++) {
		//initialize our touch array
		this.ongoingTouches[i] = stampit.mixIn({id: i}, this.mixInState, this.mixInInstanceMethods);
	}

	this.getFirstFree = function() {
		var i;
		for(i = 0; i < touchArraySize; i++) {
			if(this.ongoingTouches[i].nativeId == null) {
				return i;
			}
		}
		return undefined;
	}

	//get the touch
	this.get = function(i) {
		return this.ongoingTouches[i];
	}

	this.getAll = function() {
		return this.ongoingTouches;
	}

	//get the touch by its native id
	this.getByNativeId = function(id) {
		var k;
		for(k = 0; k < touchArraySize; k++) {
			if(this.ongoingTouches[k].nativeId == id) {
				return this.ongoingTouches[k];
			}
		}
	}

	//put a touch
	this.put = function(touchEvt) {
		var i;

		i = this.getFirstFree();
		if (i != undefined) {
			this.ongoingTouches[i].pageX = touchEvt.pageX;
			this.ongoingTouches[i].pageY = touchEvt.pageY;
			var stageInfo = KineticScreenAutoSize.sizeInfo();
			var adjustedX = touchEvt.pageX - stageInfo.xToCenter;
			var adjustedY = touchEvt.pageY - stageInfo.yToCenter;
			this.ongoingTouches[i].stageX = adjustedX / stageInfo.xScale;
			this.ongoingTouches[i].stageY = adjustedY / stageInfo.yScale;
			this.ongoingTouches[i].hitX = adjustedX;
			this.ongoingTouches[i].hitY = adjustedY;
			this.ongoingTouches[i].nativeId = touchEvt.identifier;
			this.ongoingTouches[i].observers = [];
			this.size++;
			// id stays untouched.
			return this.ongoingTouches[i];
		}
		return undefined;
	}

	this.putAt = function(touchEvt, i) {
		
		this.ongoingTouches[i].pageX = touchEvt.pageX;
		this.ongoingTouches[i].pageY = touchEvt.pageY;
		var stageInfo = KineticScreenAutoSize.sizeInfo();
		var adjustedX = touchEvt.pageX - stageInfo.xToCenter;
		var adjustedY = touchEvt.pageY - stageInfo.yToCenter;
		this.ongoingTouches[i].stageX = adjustedX / stageInfo.xScale;
		this.ongoingTouches[i].stageY = adjustedY / stageInfo.yScale;
		this.ongoingTouches[i].hitX = adjustedY;
		this.ongoingTouches[i].hitY = adjustedY;
		this.ongoingTouches[i].nativeId = touchEvt.identifier;
		this.ongoingTouches[i].observers = [];
		// id stays untouched.
		return i;
	}


	this.updateAt = function(touchEvt, i) {
		var k;
		for(k = 0; k < touchArraySize; k++) {
			if(this.ongoingTouches[k].id == i) {
				this.ongoingTouches[k].pageX = touchEvt.pageX;
				this.ongoingTouches[k].pageY = touchEvt.pageY;
				var stageInfo = KineticScreenAutoSize.sizeInfo();
				var adjustedX = touchEvt.pageX - stageInfo.xToCenter;
				var adjustedY = touchEvt.pageY - stageInfo.yToCenter;
				this.ongoingTouches[k].stageX = adjustedX / stageInfo.xScale;
				this.ongoingTouches[k].stageY = adjustedY / stageInfo.yScale;
				this.ongoingTouches[k].hitX = adjustedY;
				this.ongoingTouches[k].hitY = adjustedY;
				return k;
			}
		}
	}

	this.updateAtNativeId = function(touchEvt, id) {
		var k;
		for(k = 0; k < touchArraySize; k++) {
			if(this.ongoingTouches[k].nativeId == id) {
				this.ongoingTouches[k].pageX = touchEvt.pageX;
				this.ongoingTouches[k].pageY = touchEvt.pageY;
				var stageInfo = KineticScreenAutoSize.sizeInfo();
				var adjustedX = touchEvt.pageX - stageInfo.xToCenter;
				var adjustedY = touchEvt.pageY - stageInfo.yToCenter;
				this.ongoingTouches[k].stageX = adjustedX / stageInfo.xScale;
				this.ongoingTouches[k].stageY = adjustedY / stageInfo.yScale;
				this.ongoingTouches[k].hitX = adjustedY;
				this.ongoingTouches[k].hitY = adjustedY;
				return this.ongoingTouches[k];
			}
		}
	}

	//remove a touch
	this.remove = function(i) {
		//set to null to 'delete'
		this.ongoingTouches[i].nativeId = null;
	}

	//remove a touch
	this.removeByNativeId = function(id) {
		var k;
		for(k = 0; k < touchArraySize; k++) { 
			if(this.ongoingTouches[k].nativeId == id) {
				//set to null to 'delete'
				this.ongoingTouches[k].nativeId = null;
				this.size--;
				return this.ongoingTouches[k];
			}
		}
	}


	//iterator logic
	this.next = function() {
		var k;
		for(k = itIndex; i < touchArraySize; k++) {
			if(this.ongoingTouches[k].nativeId != null) {
				return this.ongoingTouches[k];
			}
			this.itIndex++;
		}
		this.reset();
		return null;
	}
	this.reset = function() {
		this.itIndex = 0;
	}


}