var aipnetwork;
window.__AIPCLIENT_DEVELOPER_MODE = true;

document.getElementById("mobicodesubmit").onmousedown= function (e) {
	document.getElementById("mobicode").style.visibility="hidden";
    
    // create network object
    aipnetwork= new AIPClient.client.Network();

    // connect to network (this will only work if mobicode is valid, meaning that server is connected)
	aipnetwork.connect(document.getElementById("mobicode").value);

    // listen to helloback event sent to you from the server
    aipnetwork.on('helloback', function(packet) {
    	    
    	    //the packet include all Dictionary<key,value> entries from the server, 
    	    //which in javascript can be read as properties of packet "object"
			hello_back_message = packet.hellobackmessage;
		    alert(hello_back_message);
    });
    
    // switch interface from mobicode input to game interface (buttons etc.)
    this.style.visibility="hidden";
    document.getElementById("btnStart").style.visibility="visible";   
    document.getElementById("btnStart").style.display="block";  
    document.getElementById("btnEnd").style.visibility="visible";   
    document.getElementById("btnEnd").style.display="block";  

                document.getElementById("info").innerHTML = "alpha: "  + 0.0 + ", beta: " + 0.0 + ", gamma: " + 0.0;

    if (window.DeviceOrientationEvent) {
        console.log("DeviceOrientationEvent");
        window.addEventListener('deviceorientation', function(event) {
            if(isCapturing
                && (Math.abs(prevAlphaSent - event.alpha) > sendSensitivity
                || Math.abs(prevBetaSent - event.beta) > sendSensitivity
                || Math.abs(prevGammaSent - event.gamma) > sendSensitivity))
                 {
                    prevAlphaSent = event.alpha;
                    prevBetaSent = event.beta;
                    prevGammaSent = event.gamma;
                console.log(event.alpha + "  " + event.beta + "  " + event.gamma);
                aipnetwork.send("OrientationEvent", "{\"alpha\": " + event.alpha + ", \"beta\": " + event.beta + ", \"gamma\": " + event.gamma + "}");
                document.getElementById("info").innerHTML = "alpha: "  + event.alpha + ", beta: " + event.beta + ", gamma: " + event.gamma;
            }  
        }
        , true);
    }
    
}

var isCapturing = false;
var prevAlphaSent = 0.0;
var prevBetaSent = 0.0;
var prevGammaSent = 0.0;
var sendSensitivity = 2.0;

document.getElementById("btnStart").onclick= function (e) {
    isCapturing = true;
}
document.getElementById("btnEnd").onclick= function (e) {
    isCapturing = false;
}
