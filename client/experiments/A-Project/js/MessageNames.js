var MessageNames = (function(){
	return {
		Ping: {
			name: "MessagePing",
			clientTime: 1
		},

		PingBack: {
			name: "MessagePingBack",
			clientTime: 1,
			serverTime: 1
		}
	};
});