var aipnetwork;
window.__AIPCLIENT_DEVELOPER_MODE = true;

document.getElementById("mobicodesubmit").onmousedown= function (e) {
	document.getElementById("mobicode").style.visibility="hidden";
    
    // create network object
    aipnetwork= new AIPClient.client.Network();

    // connect to network (this will only work if mobicode is valid, meaning that server is connected)
	aipnetwork.connect(document.getElementById("mobicode").value);

    // listen to helloback event sent to you from the server
    aipnetwork.on('helloback', function(packet) {
    	    
    	    //the packet include all Dictionary<key,value> entries from the server, 
    	    //which in javascript can be read as properties of packet "object"
			hello_back_message = packet.hellobackmessage;
		    alert(hello_back_message);
    });
    
    // switch interface from mobicode input to game interface (buttons etc.)
    this.style.visibility="hidden";
	document.getElementById("tapme").style.visibility="visible";   
	document.getElementById("tapme").style.display="block";  
    
}

document.getElementById("tapme").onclick= function (e) {
	aipnetwork.send("helloevent", "{message: 'Hello Server!', movingX: 1, movingY: -1}");
}
