//Adjusts the HTML 5 Elements
var adjustHTMLFiveElements = function() {

  //Set the HTML elements based on the Kinetic canvas size
  $( "#BallsTitle" ).css( "top", (KineticScreenAutoSize.sizeInfo().yToCenter+KineticScreenAutoSize.sizeInfo().height*.1)+ 'px' );
  $( ".styled-select-names" ).css( "top", (KineticScreenAutoSize.sizeInfo().yToCenter+KineticScreenAutoSize.sizeInfo().height*.3)+ 'px' );
  $( ".styled-select-colours" ).css( "top", (KineticScreenAutoSize.sizeInfo().yToCenter+KineticScreenAutoSize.sizeInfo().height*.5)+ 'px' );
  $( "#codeInput" ).css( "top", (KineticScreenAutoSize.sizeInfo().yToCenter+KineticScreenAutoSize.sizeInfo().height*.7)+ 'px' );
  $( ".sendMessage" ).css( "top", (KineticScreenAutoSize.sizeInfo().yToCenter+KineticScreenAutoSize.sizeInfo().height*.8)+ 'px' );
};

//Function hides all of the HTML5 elements when the button is clicked
var submitDataAndHideHTMLFiveElements = function(){
	//All of the fields we are going to be sending
	var nameChoice = $(".nameOption").find(":selected").text();
	var colourChoice = $(".colourOption").find(":selected").text();
	var codeEntry1 = $("#entry1").val();
	var codeEntry2 = $("#entry2").val();
	var codeEntry3 = $("#entry3").val();
	var codeEntry4 = $("#entry4").val();
	var codeEntry5 = $("#entry5").val();

	console.log('name '+nameChoice);
	console.log('colour '+colourChoice);
	console.log('codeEntry1 '+codeEntry1);
	console.log('codeEntry2 '+codeEntry2);
	console.log('codeEntry3 '+codeEntry3);
	console.log('codeEntry4 '+codeEntry4);
	console.log('codeEntry5 '+codeEntry5);

	$( "#openingPage-container" ).css('visibility', 'hidden');

	//adds the first loading layer
	addCorrectLayer(0);

};

var  setFocusForInputs = function(){
	$(".codeEntry").keyup(function () {
		$(this).next(".codeEntry").focus();
	});
};

//Removes the previous layer and sets up the next layer
var addCorrectLayer = function(layerNum){
    console.log('next button clicked');
    window. stage = KineticScreenAutoSize.stage();
    var node = stage.find('#theLayer');
    node.remove();
// (someText1, someText2, xNum, yNum, nextLayer)
	switch(layerNum) {
    case 0:
		stage.add(LayerCookieCutter("Loading...",null, 50, 100, 1));
        break;
    case 1:
        stage.add(LayerCookieCutter("Player Draws the Line!!!","\n\nYousifs magic goes here!!!", -75, 70, 2));
        break;
    case 2:
        stage.add(LayerCookieCutter("Round 1",null, 50, 100, 3));
        break;
    case 3:
        stage.add(LayerCookieCutter("Sit Back","\n\nAnd Relax!",0, 70, 4));
        break;
    case 4:
        stage.add(LayerCookieCutter("Yousif Again", "\n\nPlace Controller Magic Here!!", -75, 70, 5));
        break;
    case 5:
        stage.add(LayerCookieCutter("End Of Round 1",null, 50, 100, 6));
        break;
    case 6:
        stage.add(LayerCookieCutter("End Of Game Credits",null, 50, 100, null));
    default:
        break;
}
}