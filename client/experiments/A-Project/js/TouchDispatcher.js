//Singleton
var TouchDispatcher = function() {

	this.listeners = [];

	this.handleStart = function(evt) {
		var i;
		for(i = 0; i < this.listeners.length; i++) {
			this.listeners[i].touchstart(evt);
		}
	}
	this.handleMove = function(evt) {
		var i;
		for(i = 0; i < this.listeners.length; i++) {
			this.listeners[i].touchmove(evt);
		}
	}
	this.handleEnd = function(evt) {
		var i;
		for(i = 0; i < this.listeners.length; i++) {
			this.listeners[i].touchend(evt);
		}
	}
	this.handleCancel = function(evt) {
		var i;
		for(i = 0; i < this.listeners.length; i++) {
			this.listeners[i].touchcancel(evt);
		}
	}
	this.addAsListener = function(layer) {
		this.listeners.push(layer);
	}

	this.removeAsListener = function(layer) {
		var i;
		for(i = 0; i < this.listeners.length; i++) {
			if(this.listeners[i] == layer) {
				this.listeners.splice(i,1); //remove the listener
			}
		}
	}


}