

var LayerCookieCutter = function(someText1, someText2, xNum, yNum, nextLayer) {
    if(someText2==null)
    {
        someText2 = "";
    }
    console.log("Loading Layer which is the First Layer");
    window. stage = KineticScreenAutoSize.stage();
	//Create a layer and add the loading text to it
    var layer = new Kinetic.Layer();
    layer.id('theLayer');
    var simpleText = new Kinetic.Text({
        x: stage.width() / 2+xNum,
        y: KineticScreenAutoSize.sizeInfo().yToCenter+yNum,
        text: someText1+someText2,
        fontSize: 50,
        fontFamily: 'Calibri',
        fill: 'white'
    });

    layer.add(simpleText);
    if(nextLayer!=null)
    {
        var group = new Kinetic.Group({});
        //Button just for flow
        var rect = new Kinetic.Rect({
          x:KineticScreenAutoSize.sizeInfo().width/2+75,y:KineticScreenAutoSize.sizeInfo().height-100,
          width:100,
          height:50,
          fill:'#ffff00',
          listening:false
        });
        var buttonText = new Kinetic.Text({
          x: KineticScreenAutoSize.sizeInfo().width/2+75 ,
          y: KineticScreenAutoSize.sizeInfo().height-90,
          text: 'Next Screen',
          fontSize: 20,
          fontFamily: 'Calibri',
          fill: 'black'
        });
        var buttonRect = new Kinetic.Rect({
          x:KineticScreenAutoSize.sizeInfo().width/2+75,y:KineticScreenAutoSize.sizeInfo().height-100,
          width:100,
          height:50,
          listening:true
        });

        buttonRect.on("click" ,function handleClick(){
            console.log('next button clicked');
            addCorrectLayer(nextLayer);
        });

        group.add(rect);
        group.add(buttonText);
        group.add(buttonRect);
        layer.add(group);
    }
    
    return layer;
}
