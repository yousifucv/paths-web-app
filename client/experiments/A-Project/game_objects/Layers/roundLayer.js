
var RoundLayer = function(){

var networkObject = {};

var layerLineConfig = {
	listening: true, 
	id:'roundLayer'
};

var interfaceLayer = stampit.mixIn(new Kinetic.Layer(layerLineConfig), networkObject);

var simpleTextConfig = new Kinetic.Text({
        x: stage.width() / 2,
        y: KineticScreenAutoSize.sizeInfo().yToCenter,
        text: "Round Layer",
        fontSize: 50,
        fontFamily: 'Calibri',
        fill: 'white'
});
var simpleText = new Kinetic.Text(simpleTextConfig);

   interfaceLayer.add(simpleText);


var group = new Kinetic.Group({});

var rectConfig = {
	x:KineticScreenAutoSize.sizeInfo().width/2+75,y:KineticScreenAutoSize.sizeInfo().height-100,
	width:100,
	height:50,
	fill:'#ffff00',
	listening:false
};
var rect = new Kinetic.Rect(rectConfig);
group.add(rect);


var buttonTextConfig = {
	x: KineticScreenAutoSize.sizeInfo().width/2+75 ,
	y: KineticScreenAutoSize.sizeInfo().height-90,
	text: 'Next Screen',
	fontSize: 20,
	fontFamily: 'Calibri',
	fill: 'black'
}
var buttonText = new Kinetic.Text(buttonTextConfig);
group.add(buttonText);

var buttonRectConfig ={
	x:KineticScreenAutoSize.sizeInfo().width/2+75,y:KineticScreenAutoSize.sizeInfo().height-100,
	width:100,
	height:50,
	listening:true
};

var button = new Kinetic.Rect(buttonRectConfig);
group.add(button);


button.on("click" ,function handleClick(){
	console.log('next button clicked');
	addCorrectLayer(nextLayer);
});


interfaceLayer.add(group);

return interfaceLayer;
	
}




