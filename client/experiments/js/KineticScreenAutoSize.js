var KineticScreenAutoSize = (function() {
  var self = {
    $container: null,
    stage: null,
    baseWidth: 0,
    baseHeight: 0,
    sizeInfo: undefined, //defined below
    maxWidth: Number.MAX_VALUE,
    maxHeight: Number.MAX_VALUE
  };

  var init = function(selector, baseWidth, baseHeight) {
    self.$container = $(selector);
    self.baseWidth = baseWidth;
    self.baseHeight = baseHeight;
    initStage();
  };

  var initStage = function($container) {
    self.stage = new Kinetic.Stage({
      container: 'stage-autosize',
      width: self.baseWidth,
      height: self.baseHeight
    });
  };
//Ed says this is great!!!!!!!!!!!!!
  /*
   * Screen-Sizing Methods
   */
  var autoSize = function() {  
    // Style required for resizeScreen below
    self.$container.css({
      position: 'absolute',
      left: '50%',
      top: '50%',
      width: '100%',
      height: '100%'
    });

    // Resize automatically
    window.addEventListener('resize', resizeStageToFitScreen, false);
    window.addEventListener('orientationchange', resizeStageToFitScreen, false);

    // Resize
    resize = resizeStageToFitScreen();
  };

  var resizeStageToFitScreen = function() {
    /*
     * Following directions here: http://stackoverflow.com/a/19645405/1093087
     */
    var resized = calculateResize();

    // Resize the kinetic container element proportionally
    resized.cssSettings = {
      left: resized.xToCenter + 'px',
      top: resized.yToCenter + 'px',
      width: resized.width,
      height: resized.height,
    }
    self.$container.css(resized.cssSettings);

    // Let Kinetic know its container is resizing with .setWidth and .setHeight
    self.stage.setSize(resized);

    // Use .setScaleX and setScaleY followed by stage.draw to scale the stage
    // and all its components.
    // self.stage.scaleX(resized.xScale);
    // self.stage.scaleY(resized.yScale);
    self.stage.setScale({x:resized.xScale,y:resized.yScale});

    self.stage.draw();
    self.sizeInfo = resized;
    return resized;
  };

  var calculateResize = function() {
    var resized = {
      width: 0,
      height: 0,
      stageWidth: 0,
      stageHeight: 0,
      xScale: 0,
      yScale: 0,
      xToCenter: 0,
      yToCenter: 0
    }

    // var windowWidth = (window.innerWidth > 700) ? 700 : window.innerWidth,
    var windowWidth = (window.innerWidth > self.maxWidth) ? self.maxWidth : window.innerWidth,
        windowHeight = (window.innerHeight > self.maxHeight) ? self.maxHeight : window.innerHeight,
        desiredWidthToHeightRatio = self.baseWidth / self.baseHeight,
        currentWidthToHeightRatio = windowWidth / windowHeight;
    if ( currentWidthToHeightRatio > desiredWidthToHeightRatio ) {
      resized.width = windowHeight * desiredWidthToHeightRatio;
      resized.height = windowHeight;
    }
    else {
      resized.width = windowWidth;
      resized.height = windowWidth / desiredWidthToHeightRatio;
    }
    resized.xToCenter = (window.innerWidth - resized.width) /2;
    resized.yToCenter = (window.innerHeight - resized.height) /2;//* 0;/// 2;
    resized.xScale = resized.width/self.baseWidth,
    resized.yScale = resized.height/self.baseHeight;
    resized.stageWidth = resized.width / resized.xScale;
    resized.stageHeight = resized.height / resized.yScale;
    return resized;
  };

  /*
   * Public API
   */
  var publicAPI = {
    init: init,
    stage: function() { return self.stage; },
    sizeInfo: function() { return self.sizeInfo; },
    autoSize: autoSize,
    setMaxWidth: function(max) {self.maxWidth = max;},
    setMaxHeight: function(max) {self.maxHeight = max;}
  }

  return publicAPI;

})();