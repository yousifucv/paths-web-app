var aipnetwork;
window.__AIPCLIENT_DEVELOPER_MODE = true;

document.getElementById("mobicodesubmit").onmousedown= function (e) {
	document.getElementById("mobicode").style.visibility="hidden";
    
    // create network object
    aipnetwork= new AIPClient.client.Network();

    // connect to network (this will only work if mobicode is valid, meaning that server is connected)
	aipnetwork.connect(document.getElementById("mobicode").value);

    // listen to helloback event sent to you from the server
    aipnetwork.on('helloback', function(packet) {
    	    
    	    //the packet include all Dictionary<key,value> entries from the server, 
    	    //which in javascript can be read as properties of packet "object"
			hello_back_message = packet.hellobackmessage;
		    alert(hello_back_message);
    });
    
    // switch interface from mobicode input to game interface (buttons etc.)
    this.style.visibility="hidden";
    document.getElementById("btnStart").style.visibility="visible";   
    document.getElementById("btnStart").style.display="block";  
    document.getElementById("btnEnd").style.visibility="visible";   
    document.getElementById("btnEnd").style.display="block";  

    document.getElementById("orientInfo").innerHTML = "alpha: "  + 0.0 + ", beta: " + 0.0 + ", gamma: " + 0.0;

    if (window.DeviceOrientationEvent) {
        console.log("DeviceOrientationEvent available");
        document.getElementById("orientInfo").innerHTML = "DeviceOrientationEvent available";
        window.addEventListener('deviceorientation', function(event) {
            if(isCapturing
                && (Math.abs(prevAlphaSent - event.alpha) > sendSensitivity
                || Math.abs(prevBetaSent - event.beta) > sendSensitivity
                || Math.abs(prevGammaSent - event.gamma) > sendSensitivity))
                 {
                    prevAlphaSent = event.alpha;
                    prevBetaSent = event.beta;
                    prevGammaSent = event.gamma;
                console.log(event.alpha + "  " + event.beta + "  " + event.gamma);
                aipnetwork.send("OrientationEvent", "{\"alpha\": " + event.alpha + ", \"beta\": " + event.beta + ", \"gamma\": " + event.gamma + "}");
                document.getElementById("orientInfo").innerHTML = "alpha: "  + event.alpha + ", beta: " + event.beta + ", gamma: " + event.gamma;
            }  
        }
        , true);
    }
    if(window.DeviceMotionEvent) {
         window.addEventListener('devicemotion', function(event) {
            if(isCapturing)
            {
                var acc = event.acceleration;
                var accG = event.accelerationIncludingGravity;
                var rot = event.rotationRate;

                var packet = {};
                packet.acceleration = {};
                packet.rotationRate = {};

                packet.acceleration.x = event.acceleration.x;
                packet.acceleration.y = event.acceleration.y;
                packet.acceleration.z = event.acceleration.z;

                packet.rotationRate.alpha = event.rotationRate.alpha;
                packet.rotationRate.beta = event.rotationRate.beta;
                packet.rotationRate.gamma = event.rotationRate.gamma;
                
                packet.interval = event.interval;

                aipnetwork.send("AccelEvent", JSON.stringify(packet));
                var info = "";
                info += "acceleration x: " + acc.x + "</br>";
                info += "acceleration y: " + acc.y + "</br>";
                info += "acceleration z: " + acc.z + "</br>";
                info += "accelerationWithG x: " + accG.x + "</br>";
                info += "accelerationWithG y: " + accG.y + "</br>";
                info += "accelerationWithG z: " + accG.z + "</br>";
                info += "rotationRate x: " + rot.alpha + "</br>";
                info += "rotationRate y: " + rot.beta + "</br>";
                info += "rotationRate z: " + rot.gamma + "</br>";
                info += "interval: " + event.interval;
                info = JSON.stringify(packet);
                document.getElementById("accelInfo").innerHTML = info;
            }  
        }
        , true);
    }
    aipnetwork.on('Hello', function(packet) {
            alert("Hello");
    });
    
}

var isCapturing = false;
var prevAlphaSent = 0.0;
var prevBetaSent = 0.0;
var prevGammaSent = 0.0;
var sendSensitivity = 2.0;

document.getElementById("btnStart").onclick= function (e) {
    isCapturing = true;
}
document.getElementById("btnEnd").onclick= function (e) {
    isCapturing = false;
}
